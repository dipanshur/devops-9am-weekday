module "vpc1" {
  source = "./modules/iaac"

  identifier = "tg-devops-9am"
  vpc_cidr = "172.31.0.0/16"
  CreatedBy = "Ashish"
  public_subnet_az = "us-east-1a"
  private_subnet_az = "us-east-1b"
  private_subnet_cidr = "172.31.1.0/24"
  public_subnet_cidr = "172.31.2.0/24"
}


module "vpc2" {
  source = "./modules/iaac"

  identifier = "tg-devops-10am"
  vpc_cidr = "172.31.0.0/16"
  CreatedBy = "Dipanshu"
  public_subnet_az = "us-east-1a"
  private_subnet_az = "us-east-1b"
  private_subnet_cidr = "172.31.1.0/24"
  public_subnet_cidr = "172.31.2.0/24"
}

module "vpc3" {
  source = "./modules/iaac"

  identifier = "tg-devops-11am"
  vpc_cidr = "192.168.0.0/16"
  CreatedBy = "Ankur"
  public_subnet_az = "us-east-1a"
  private_subnet_az = "us-east-1b"
  private_subnet_cidr = "192.168.1.0/24"
  public_subnet_cidr = "192.168.2.0/24"
}


module "vpc4" {
  source = "./modules/iaac"

  identifier = "tg-devops-8am"
  vpc_cidr = "192.168.0.0/16"
  CreatedBy = "jaggu"
  public_subnet_az = "us-east-1a"
  private_subnet_az = "us-east-1b"
  private_subnet_cidr = "192.168.1.0/24"
  public_subnet_cidr = "192.168.2.0/24"
}