terraform {
  backend "s3" {
    bucket = "devops-batch9-tfstate"
    key = "tfstate/terraform.tfstate"
    region = "us-east-1"
  }
}