variable "vpc_cidr" {
  description = "Provide cidr to define VPC"
  default = "10.0.0.0/16"
  
}

variable "identifier" {
  description = "batch ID"
  
}

variable "CreatedBy" {
  description = "Name of Developer"
  
}

variable "public_subnet_cidr" {
    description = "Public Subnet CIDR"
  
}

variable "public_subnet_az" {
  description = "Public Subnet AZ"
}

variable "private_subnet_cidr" {
  description = "Private Subnet CIDR"
  
}

variable "private_subnet_az" {
    description = "Private Subnet AZ"
  
}