resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    "Name" = "${var.identifier}-terraform-vpc"
    "CreatedBy" = var.CreatedBy
  }
}

resource "aws_subnet" "public_subnet" {
   # vpc_id = "vpc-07970f0e3e1b666c0"   Hard Code
   vpc_id = aws_vpc.vpc.id
   cidr_block = var.public_subnet_cidr
   availability_zone = var.public_subnet_az
   tags = {
    "Name" = "${var.identifier}-terraform-public-subnet"
    "CreatedBy" = var.CreatedBy
  }
  
}

resource "aws_subnet" "private_subnet" {
   # vpc_id = "vpc-07970f0e3e1b666c0"   Hard Code
   vpc_id = aws_vpc.vpc.id
   cidr_block = var.private_subnet_cidr
   availability_zone = var.private_subnet_az
   tags = {
    "Name" = "${var.identifier}-terraform-private-subnet"
    "CreatedBy" = var.CreatedBy
  }
  
}

resource "aws_route_table" "public_rt" {
    vpc_id = aws_vpc.vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }

    tags = {
    "Name" = "${var.identifier}-terraform-public-rt"
    "CreatedBy" = var.CreatedBy
  }
  
}

resource "aws_route_table" "private_rt" {
    vpc_id = aws_vpc.vpc.id
    # route {
    #     cidr_block = "0.0.0.0/0"
    #     gateway_id = aws_nat_gateway.public_nat.id
    # }
    tags = {
    "Name" = "${var.identifier}-terraform-private-rt"
    "CreatedBy" = var.CreatedBy
  }
  
}

resource "aws_route_table_association" "public-rt-association" {
    subnet_id = aws_subnet.public_subnet.id
    route_table_id = aws_route_table.public_rt.id
  
}

resource "aws_route_table_association" "private-rt-association" {
    subnet_id = aws_subnet.private_subnet.id
    route_table_id = aws_route_table.private_rt.id
  
}

resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.vpc.id
    tags = {
    "Name" = "${var.identifier}-terraform-igw"
    "CreatedBy" = var.CreatedBy
  }
  
}
/*
resource "aws_eip" "eip" {
  domain = "vpc"
  tags = {
    "Name" = "${var.identifier}-terraform-eip"
    "CreatedBy" = var.CreatedBy
  }
}

resource "aws_nat_gateway" "public_nat" {
  connectivity_type = "public"
  subnet_id = aws_subnet.private_subnet.id
  allocation_id = aws_eip.eip.id
}*/