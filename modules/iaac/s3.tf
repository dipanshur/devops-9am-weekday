resource "aws_s3_bucket" "tg-devops-bucket" {
  bucket = "${var.identifier}-terraform-bucke-us"
  object_lock_enabled = false
  tags = {
    "Name" = "${var.identifier}-terraform-bucket-us",
    "Created By" = var.CreatedBy
  }
}

resource "aws_s3_bucket" "tg-devops-bucket-eu" {
  provider = aws.eu
  bucket = "${var.identifier}-terraform-bucket-eu"
  object_lock_enabled = true
  tags = {
    "Name" = "${var.identifier}-terraform-bucket-eu",
    "Created By" = var.CreatedBy
  }
}
resource "aws_s3_bucket" "tg-devops-bucket-eu2" {
  provider = aws.eu
  bucket = "${var.identifier}-terraform-bucket-eu2"
  object_lock_enabled = true
  tags = {
    "Name" = "${var.identifier}-terraform-bucket-eu2",
    "Created By" = var.CreatedBy
  }
}
